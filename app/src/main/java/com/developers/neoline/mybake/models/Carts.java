package com.developers.neoline.mybake.models;

import java.io.Serializable;

/**
 * Created by savad on 22/12/17.
 */

public class Carts implements Serializable {
    public int id;
    public int proId;
    public int slno;
    public int quantity;
    public int isNormal;
    public int posVal;
    public double line_price;
    public String product_name;
    public String price;

    public Carts(String product_name, String price, int quantity, int slno, int posVal) {

        this.product_name = product_name;
        this.price = price;
        this.quantity = quantity;
        this.line_price = Integer.parseInt(price) * quantity;
        this.slno = slno;
        this.posVal = posVal;

    }

    public Carts() {

    }

    public Carts(String proid, String product_name, int quantity, String price) {

        this.product_name = product_name;
        this.quantity = quantity;
        this.price = price;
        this.line_price = Double.parseDouble(price) * quantity;
        this.proId = Integer.parseInt(proid);

    }

    public int getProId() {

        return proId;
    }

    public void setProId(int proId) {

        this.proId = proId;
    }

//    public void addToCart(String product_name, String price, int quantity, Context mcontext) {
//        List<String> strings = new ArrayList<>()
//        Utility.setPrefs("product_name", product_name, mcontext);
//        Utility.setPrefs("price", price, mcontext);
//        Utility.setPrefs("quantity", String.valueOf(quantity), mcontext);
//        Utility.setPrefs("line_price", String.valueOf(Integer.parseInt(price) * quantity), mcontext);
//
//    }
//
//    public List<Carts> getCarts() {
//        Utility.setPrefs("product_name", product_name, mcontext);
//        Utility.setPrefs("price", price, mcontext);
//        Utility.setPrefs("quantity", String.valueOf(quantity), mcontext);
//        Utility.setPrefs("line_price", String.valueOf(Integer.parseInt(price) * quantity), mcontext);
//
//
//    }


    public int getIsNormal() {
        return isNormal;
    }

    public void setIsNormal(int isNormal) {
        this.isNormal = isNormal;
    }

    public String getProduct_name() {

        return product_name;
    }

    public void setProduct_name(String productName) {

        this.product_name = productName;
    }

    public String getPrice() {

        return price;
    }

    public void setPrice(String price) {

        this.price = price;
    }

    public double getLineTotal() {
        if (getIsNormal() == 2) {

            return 0;
        } else {
            return line_price;
        }
    }

    public void setLineTotal(int lineTotal) {

        this.line_price = lineTotal;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public int getQTy() {

        return quantity;
    }

    public void settQuantity(int qty) {
        this.quantity = qty;
    }

    public int getPosVal() {

        return posVal;
    }
}
