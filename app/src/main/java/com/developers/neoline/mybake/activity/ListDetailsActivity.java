package com.developers.neoline.mybake.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.adapter.ListDetailsAdapter;
import com.developers.neoline.mybake.models.CartList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListDetailsActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 1;
    private String key;
    private DatabaseReference mDatabase, myRef;
    private List<CartList> cartListsNormal = new ArrayList<>(), cartListsGr = new ArrayList<>(), cartLists = new ArrayList<>();
    private ProgressBar PbLoading;
    private RecyclerView RvFinalNormalList, RvFinalGrList;
    private RecyclerView.Adapter mListDetailsAdapter, nListDetailsAdapter;
    private TextView TvSubTotal, TvRouteName, TvShopName, TvNormalTotal, TvGrTotal, TvVatAmount;
    private Button actPrint, actSave;
    private String routeName, shopName, resrouteName, resshopName;
    private double sumNormal, sumGr, sumNet;
    private ImageView actEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_details);


        TvSubTotal = (TextView) findViewById(R.id.tv_sub_total);
        TvRouteName = (TextView) findViewById(R.id.tv_route_name);
        TvShopName = (TextView) findViewById(R.id.tv_shop_name);
        TvNormalTotal = (TextView) findViewById(R.id.tv_normal_total);
        TvGrTotal = (TextView) findViewById(R.id.tv_gr_total);
        TvVatAmount = (TextView) findViewById(R.id.tv_vat_amount);
        actPrint = (Button) findViewById(R.id.btn_print);
        actSave = (Button) findViewById(R.id.btn_save);
        actEdit = (ImageView) findViewById(R.id.btn_edit_cart);

        PbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        key = getIntent().getStringExtra("key");

        mDatabase = FirebaseDatabase.getInstance().getReference("/list_content/" + key);

        mListDetailsAdapter = new ListDetailsAdapter(ListDetailsActivity.this, cartListsNormal);
        nListDetailsAdapter = new ListDetailsAdapter(ListDetailsActivity.this, cartListsGr);

        RvFinalNormalList = (RecyclerView) findViewById(R.id.rv_final_invoice_list);
        RvFinalGrList = (RecyclerView) findViewById(R.id.rv_final_invoice_list2);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        RvFinalNormalList.setLayoutManager(mLayoutManager);
        RvFinalNormalList.setItemAnimator(new DefaultItemAnimator());
        RvFinalNormalList.setAdapter(mListDetailsAdapter);

        RecyclerView.LayoutManager nLayoutManager = new LinearLayoutManager(this);
        RvFinalGrList.setLayoutManager(nLayoutManager);
        RvFinalGrList.setItemAnimator(new DefaultItemAnimator());
        RvFinalGrList.setAdapter(nListDetailsAdapter);


        prepareSingItemData();

        actEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), EditListDetailActivity.class);
                intent.putExtra("cartListsGr", (Serializable) cartListsGr);
                intent.putExtra("cartListsNormal", (Serializable) cartListsNormal);
                intent.putExtra("key", key);
                startActivityForResult(intent, REQUEST_CODE);

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                Log.i("here", "onActivityResut");
//                cartListsGr.clear();
//                cartListsNormal.clear();
//                mDatabase.keepSynced(true);
//                mListDetailsAdapter.notifyDataSetChanged();
//                nListDetailsAdapter.notifyDataSetChanged();
//                prepareSingItemData();
            }
        }
    }

    private void prepareSingItemData() {
        mDatabase.keepSynced(true);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                cartListsGr.clear();
                cartListsNormal.clear();
                mListDetailsAdapter.notifyDataSetChanged();
                nListDetailsAdapter.notifyDataSetChanged();
                sumGr=0;
                sumNormal=0;
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {


                    CartList cartList = dataSnapshot1.getValue(CartList.class);
                    CartList list = new CartList();
                    String name = cartList.getRoute();
                    String price = cartList.getPro_name();
                    double ln = cartList.getPro_line_total();
                    TvRouteName.setText(cartList.getRoute());
                    TvShopName.setText(cartList.getShop());

//                    Log.i("ThirdFragment", String.valueOf(ln) + "----" + price);
//                    Log.i("test", name);
//                    thumb.setName(name);
//                    thumb.setPrice(price);
//                    product = new Product(name, price);
                    if (cartList.getIsnormal() == 0) {
                        sumGr = sumGr + cartList.getPro_line_total();
                        list = new CartList(cartList.getRoute(), cartList.getShop(), cartList.getPro_name(), cartList.getPro_quantity(), cartList.getPro_price(), cartList.getPro_line_total(), 0);
                        Log.i("ThirdFragment", String.valueOf(ln) + "----" + String.valueOf(sumGr));
                        cartListsGr.add(list);
                        PbLoading.setVisibility(View.GONE);
                        nListDetailsAdapter.notifyDataSetChanged();

                    } else if (cartList.getIsnormal() == 1||cartList.getIsnormal() == 2) {
                        sumNormal = sumNormal + cartList.getPro_line_total();
                        list = new CartList(cartList.getRoute(), cartList.getShop(), cartList.getPro_name(), cartList.getPro_quantity(), cartList.getPro_price(), cartList.getPro_line_total(), cartList.getIsnormal());
                        Log.i("ThirdFragment", String.valueOf(ln) + "----" + String.valueOf(sumNormal));
                        cartListsNormal.add(list);
                        PbLoading.setVisibility(View.GONE);
                        mListDetailsAdapter.notifyDataSetChanged();
                    }


                }
                double NetTot = sumNormal - sumGr;
                double Vat = ((double) NetTot / 100.0f) * 5;
                double amountToPay = (double) NetTot + Vat;
                String formattedValue = String.format("%.2f", amountToPay);
                String formattedVat = String.format("%.2f", Vat);
                String formattedsumNormal = String.format("%.2f", sumNormal);
                String formattedsumGr = String.format("%.2f", sumGr);
                TvNormalTotal.setText(String.valueOf(formattedsumNormal));
                TvGrTotal.setText(String.valueOf(formattedsumGr));
                TvVatAmount.setText(formattedVat);
                TvSubTotal.setText(String.valueOf(formattedValue));
//                TvSubTotal.setText(String.valueOf(sumNormal));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });
//        itemListDataAdapter.notifyDataSetChanged();

    }
}
