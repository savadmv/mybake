package com.developers.neoline.mybake.util;

/**
 * Created by savad on 12/1/18.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.developers.neoline.mybake.models.Carts;
import com.google.gson.Gson;

public class SharedPreference {

    public static final String PREFS_NAME = "PRODUCT_APP";
    public static final String FAVORITES = "Product_Favorite";

    public SharedPreference() {
        super();
    }

    // This four methods are used for maintaining favorites.
    public void saveFavorites(Context context, List<Carts> favorites) {
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }
    public void emptyFevorate(Context context){
        SharedPreferences settings;
        Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();


        editor.putString(FAVORITES, null);

        editor.commit();

    }

    public void addFavorite(Context context, Carts product) {
        List<Carts> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Carts>();
        favorites.add(product);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, Carts product) {
        ArrayList<Carts> favorites = getFavorites(context);
        if (favorites != null) {
            favorites.remove(product);
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<Carts> getFavorites(Context context) {
        SharedPreferences settings;
        List<Carts> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Carts[] favoriteItems = gson.fromJson(jsonFavorites,
                    Carts[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Carts>(favorites);
        } else
            return null;

        return (ArrayList<Carts>) favorites;
    }
}