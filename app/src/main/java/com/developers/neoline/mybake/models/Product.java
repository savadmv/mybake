package com.developers.neoline.mybake.models;

import android.support.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ThrowOnExtraProperties;

/**
 * Created by savad on 21/12/17.
 */
@Keep
@IgnoreExtraProperties
public class Product {

    public String name;
    public String price;
    boolean isSelelcted;

    public Product(String name, String price) {
        this.name = name;
        this.price = price;

    }

    public Product() {

    }

    public String getPrice() {

        return price;
    }

    public void setPrice(String price) {

        this.price = price;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setIsSelected(boolean isSelelcted) {

        this.isSelelcted = isSelelcted;
    }

    public boolean IsSelected() {

        return this.isSelelcted;
    }
}
