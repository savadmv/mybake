package com.developers.neoline.mybake.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.adapter.ThumbListAdapter;
import com.developers.neoline.mybake.models.CartThumb;
import com.developers.neoline.mybake.util.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by savad on 18/12/17.
 */

public class ThirdFragment extends Fragment implements View.OnClickListener {
    private DatabaseReference mDatabase, myRef;
    private List<CartThumb> thumb_list = new ArrayList<>();
    private ProgressBar PbLoading;
    private RecyclerView RvThumbList;
    private RecyclerView.Adapter mThumbListAdapter;
    private String routeName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);
        getActivity().setTitle("Saved List");
        routeName = Utility.getPrefs("route_name", getActivity());
        mDatabase = FirebaseDatabase.getInstance().getReference("/list_thumb/" + "/" + routeName + "/");


        ////Definig  the views///////
        PbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);


        RvThumbList = (RecyclerView) view.findViewById(R.id.rv_saved_list_thumb);


        ////Intiating Adapter/////

        mThumbListAdapter = new ThumbListAdapter(getActivity(), thumb_list);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RvThumbList.setLayoutManager(mLayoutManager);
        RvThumbList.setItemAnimator(new DefaultItemAnimator());
        RvThumbList.setAdapter(mThumbListAdapter);
        prepareSingItemData();

        return view;
    }

    private void prepareSingItemData() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    CartThumb value = dataSnapshot1.getValue(CartThumb.class);
                    CartThumb thumb = new CartThumb();
                    String name = value.getShop_name();
                    String price = value.getRoute_name();

                    Log.i("ThirdFragment", name + "----" + price);
//                    Log.i("test", name);
//                    thumb.setName(name);
//                    thumb.setPrice(price);
//                    product = new Product(name, price);

                    thumb = new CartThumb(value.getKey(), value.getShop_name(), value.getRoute_name(),value.getDate(),value.getTime());

                    thumb_list.add(thumb);
                    PbLoading.setVisibility(View.GONE);
                    mThumbListAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });
//        itemListDataAdapter.notifyDataSetChanged();

    }
    private void getDropdownVaue(String keyvar, final Spinner spinner) {
        myRef.child("dropdownlist").child(keyvar).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("Main", dataSnapshot.getValue().toString());

                String[] strings = dataSnapshot.getValue(String.class).split(",");
                setDropdownVaue(strings, spinner);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void setDropdownVaue(String[] strings, Spinner spinner) {

        final List<String> plantsList = new ArrayList<>(Arrays.asList(strings));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, plantsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onPause() {
        super.onPause();
        PbLoading.setVisibility(View.GONE);
    }
}
