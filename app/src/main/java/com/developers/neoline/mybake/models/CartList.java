package com.developers.neoline.mybake.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class CartList implements Serializable {

    public String route;
    public String shop;
    public String pro_name;
    public int pro_quantity;
    public String pro_price;
    public double pro_line_total;
    public int isnormal;
    public Map<String, Boolean> stars = new HashMap<>();

    public CartList() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public CartList(String route, String shop, String pro_name, int pro_quantity, String pro_price, double pro_line_total, int isnormal) {
        this.route = route;
        this.shop = shop;
        this.pro_name = pro_name;
        this.pro_quantity = pro_quantity;
        this.pro_price = pro_price;
        this.pro_line_total = pro_line_total;
        this.isnormal = isnormal;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("route", route);
        result.put("shop", shop);
        result.put("pro_name", pro_name);
        result.put("pro_quantity", pro_quantity);
        result.put("pro_price", pro_price);
        result.put("pro_line_total", pro_line_total);
        result.put("isnormal", isnormal);


        return result;
    }
    // [END post_to_map]

    public String getRoute() {
        return route;
    }

    public String getPro_name() {
        return pro_name;
    }

    public int getPro_quantity() {
        return pro_quantity;
    }

    public String getPro_price() {
        return pro_price;
    }

    public double getPro_line_total() {
        return pro_line_total;
    }

    public String getShop() {
        return shop;
    }

    public int getIsnormal() {
        return isnormal;
    }

    public void setPro_quantity(int pro_quantity) {
        this.pro_quantity = pro_quantity;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public void setPro_price(String pro_price) {
        this.pro_price = pro_price;
    }

    public void setPro_line_total(double pro_line_total) {
        this.pro_line_total = pro_line_total;
    }

    public void setIsnormal(int isnormal) {
        this.isnormal = isnormal;
    }
}
// [END post_class]
