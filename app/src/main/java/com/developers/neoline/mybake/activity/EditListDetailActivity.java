package com.developers.neoline.mybake.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.adapter.EditListDetailsAdapter;
import com.developers.neoline.mybake.models.CartList;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditListDetailActivity extends AppCompatActivity {
    RecyclerView RvFinalNormalList, RvFinalGrList;
    EditListDetailsAdapter mListDetailsAdapter, nListDetailsAdapter;
    private String key;
    private DatabaseReference mDatabase, myRef;
    private List<CartList> cartListsNormal = new ArrayList<>(), cartListsGr = new ArrayList<>(), cartLists = new ArrayList<>();
    private ProgressBar PbLoading;
    private TextView TvSubTotal, TvRouteName, TvShopName, TvNormalTotal, TvGrTotal, TvVatAmount;
    private Button actSave, actCancel;
    private String routeName, shopName, resrouteName, resshopName;
    private double sumNormal, sumGr, sumNet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_list_detail);

        TvSubTotal = (TextView) findViewById(R.id.tv_sub_total);
        TvRouteName = (TextView) findViewById(R.id.tv_route_name);
        TvShopName = (TextView) findViewById(R.id.tv_shop_name);
        TvNormalTotal = (TextView) findViewById(R.id.tv_normal_total);
        TvGrTotal = (TextView) findViewById(R.id.tv_gr_total);
        TvVatAmount = (TextView) findViewById(R.id.tv_vat_amount);
        actCancel = (Button) findViewById(R.id.btn_cancel);
        actSave = (Button) findViewById(R.id.btn_save);
        cartListsGr = (List<CartList>) getIntent().getSerializableExtra("cartListsGr");
        cartListsNormal = (List<CartList>) getIntent().getSerializableExtra("cartListsNormal");

        PbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        key = getIntent().getStringExtra("key");

        mDatabase = FirebaseDatabase.getInstance().getReference();
        myRef = FirebaseDatabase.getInstance().getReference("/list_content/" + key);

        Log.i("key", key);

        mListDetailsAdapter = new EditListDetailsAdapter(getApplicationContext(), cartListsNormal);
        nListDetailsAdapter = new EditListDetailsAdapter(getApplicationContext(), cartListsGr);

        RvFinalNormalList = (RecyclerView) findViewById(R.id.rv_final_invoice_list);
        RvFinalGrList = (RecyclerView) findViewById(R.id.rv_final_invoice_list2);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        RvFinalNormalList.setLayoutManager(mLayoutManager);
        RvFinalNormalList.setItemAnimator(new DefaultItemAnimator());
        RvFinalNormalList.setAdapter(mListDetailsAdapter);

        RecyclerView.LayoutManager nLayoutManager = new LinearLayoutManager(this);
        RvFinalGrList.setLayoutManager(nLayoutManager);
        RvFinalGrList.setItemAnimator(new DefaultItemAnimator());
        RvFinalGrList.setAdapter(nListDetailsAdapter);

//        prepareSingItemData();
        actSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nListDetailsAdapter.getEditedList();
                myRef.removeValue();
                for (CartList cartList : nListDetailsAdapter.getEditedList()) {

//            updateInFireBase(cartList.getPro_name(), cartList.getPro_quantity(), cartList.getPro_price(), cartList.getPro_line_total(), cartList.getIsnormal());
                    updateInFireBase(cartList);
                    Log.i("edited", "----gr>" + cartList.getPro_quantity() + "-----" + cartList.getPro_line_total());
                }
                for (CartList cartList :mListDetailsAdapter.getEditedList()) {

//            updateInFireBase(cartList.getPro_name(), cartList.getPro_quantity(), cartList.getPro_price(), cartList.getPro_line_total(), cartList.getIsnormal());
                    updateInFireBase(cartList);
                    Log.i("edited", "-----nor>" + cartList.getPro_quantity() + "-----" + cartList.getPro_line_total());
                }
//        updateInFireBase(cartList.getPro_name(), cartList.getPro_quantity(), cartList.getPro_price(), cartList.getPro_line_total(), cartList.getIsnormal());
        setResultForMain();

            }
        });

        actCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent=new Intent();
               setResult(RESULT_CANCELED);
               finish();

            }
        });

    }



    private void updateInFireBase(CartList cartList) {

        String routeName = cartList.getRoute();
        String shopName = cartList.getShop();

        String subKey = mDatabase.child("list_content").child(key).push().getKey();

        Log.i("log",subKey);
        CartList post = new CartList(routeName, shopName, cartList.getPro_name(), cartList.getPro_quantity(), cartList.getPro_price(), cartList.getPro_line_total(), cartList.getIsnormal());
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/list_content/" + "/" + key + "/" + subKey, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void setResultForMain() {
        Intent intent = new Intent();
//        intent.putExtra("cartListsGr", (Serializable) nListDetailsAdapter.getEditedList());
//        intent.putExtra("cartListsNormal", (Serializable) mListDetailsAdapter.getEditedList());
        setResult(RESULT_OK, intent);
        finish();
    }
}
