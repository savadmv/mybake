package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.models.CartList;
import com.developers.neoline.mybake.util.DBHandler;

import java.util.List;

/**
 * Created by savad on 8/1/18.
 */


public class ListDetailsAdapter extends RecyclerView.Adapter<ListDetailsAdapter.MyViewHolder> {

    private List<CartList> cartItemList;
    private Context mContext;



    public ListDetailsAdapter(Context context, List<CartList> singItemList) {
        this.cartItemList = singItemList;
        this.mContext = context;
        DBHandler db = new DBHandler(mContext);
//        for (int i = 0; i < db.getShopsCount(); i++) {
//
//            Log.i("DBHandler", "Adapter---" + String.valueOf(singItemList.get(i).getProId()) + "--" + String.valueOf(singItemList.get(i).getProduct_name()) + "--" + String.valueOf(singItemList.get(i).getQTy()) + "--" + String.valueOf(singItemList.get(i).getPrice()) + "--" + String.valueOf(singItemList.get(i).getLineTotal()));
//        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.final_list_single_unit, parent, false);
        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        CartList carts = cartItemList.get(position);
        holder.TvProductName.setText(carts.getPro_name());
        holder.TvSlNo.setText(String.valueOf(position + 1));
        holder.TvUnitPrice.setText(carts.getPro_price());
        holder.TvQuantity.setText(String.valueOf(carts.getPro_quantity()));
        String formattedLineTotal = String.format("%.2f", carts.getPro_line_total());
        Log.i("testing",String.valueOf(carts.getIsnormal()));
        if (carts.getIsnormal()==2){
            holder.TvLineTot.setText("FOC");
        }
        else {
            holder.TvLineTot.setText(String.valueOf(formattedLineTotal));
        }

//        holder.LlItemLayout.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//
//                cartItemList.remove(position);
//                notifyItemRemoved(position);
//                notifyItemRangeChanged(position, cartItemList.size());
//                return false;
//            }
//        });
//        Log.i("FianList", "test------>" + carts.getProduct_name());

    }


    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvSlNo, TvProductName, TvQuantity, TvUnitPrice, TvLineTot;
        public LinearLayout LlItemLayout;


        public MyViewHolder(View view) {
            super(view);
            TvSlNo = (TextView) view.findViewById(R.id.tv_item_num);
            TvProductName = (TextView) view.findViewById(R.id.tv_product_name);
            TvQuantity = (TextView) view.findViewById(R.id.tv_quantity);
            TvUnitPrice = (TextView) view.findViewById(R.id.tv_unit_price);
            TvLineTot = (TextView) view.findViewById(R.id.tv_line_total);
            LlItemLayout = (LinearLayout) view.findViewById(R.id.list_head_layout);

        }
    }
}