package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.models.CartList;
import com.developers.neoline.mybake.util.DBHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by savad on 8/1/18.
 */


public class EditListDetailsAdapter extends RecyclerView.Adapter<EditListDetailsAdapter.MyViewHolder> {

    private List<CartList> cartItemList;
    private Context mContext;
    private List<CartList> editedList = new ArrayList<>();

    public EditListDetailsAdapter(Context context, List<CartList> singItemList) {
        this.cartItemList = singItemList;
        this.mContext = context;
        DBHandler db = new DBHandler(mContext);
//        for (int i = 0; i < db.getShopsCount(); i++) {
//
//            Log.i("DBHandler", "Adapter---" + String.valueOf(singItemList.get(i).getProId()) + "--" + String.valueOf(singItemList.get(i).getProduct_name()) + "--" + String.valueOf(singItemList.get(i).getQTy()) + "--" + String.valueOf(singItemList.get(i).getPrice()) + "--" + String.valueOf(singItemList.get(i).getLineTotal()));
//        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.edit_final_list_single_unit, parent, false);
        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CartList carts = cartItemList.get(position);
        holder.TvProductName.setText(carts.getPro_name());
        holder.TvSlNo.setText(String.valueOf(position + 1));
        holder.TvUnitPrice.setText(carts.getPro_price());
        holder.TvQuantity.setText(String.valueOf(carts.getPro_quantity()));
        String formattedLineTotal = String.format("%.2f", carts.getPro_line_total());
        if (carts.getIsnormal() == 2) {
            holder.TvLineTot.setText("FOC");

        } else {
            holder.TvLineTot.setText(String.valueOf(formattedLineTotal));
        }


        final CartList cartList = new CartList(carts.getRoute(), carts.getShop(), carts.getPro_name(), carts.getPro_quantity(), carts.getPro_price(), carts.getPro_line_total(), carts.getIsnormal());
        cartList.setRoute(carts.getRoute());
        cartList.setShop(carts.getShop());
        cartList.setPro_name(carts.getPro_name());
        cartList.setPro_price(carts.getPro_price());
        cartList.setIsnormal(carts.getIsnormal());
        holder.actRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartItemList.remove(position);
                editedList.clear();
                notifyDataSetChanged();
            }
        });

        holder.TvQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                Log.i("value", "--->" + String.valueOf(charSequence.length()));
                if (charSequence.length() != 0) {

                    double newQUantity = Double.parseDouble(charSequence.toString());
                    double newLineTo = Double.parseDouble(carts.getPro_price()) * newQUantity;
                    String formattednewLineTo = String.format("%.2f", newLineTo);
                    if (carts.getIsnormal() != 2) {
                        holder.TvLineTot.setText(String.valueOf(formattednewLineTo));
                    }
                    cartList.setPro_quantity(Integer.parseInt(charSequence.toString()));
                    cartList.setPro_line_total(newLineTo);


                }


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
        editedList.add(cartList);

//        holder.LlItemLayout.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//
//                cartItemList.remove(position);
//                notifyItemRemoved(position);
//                notifyItemRangeChanged(position, cartItemList.size());
//                return false;
//            }
//        });
//        Log.i("FianList", "test------>" + carts.getProduct_name());

    }

    public void refreshUI(List<CartList> cartList, int isnormal) {


    }

    public List<CartList> getEditedList() {

        return editedList;
    }

    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvSlNo, TvProductName, TvQuantity, TvUnitPrice, TvLineTot;
        public LinearLayout LlItemLayout;
        public ImageView actRemoveItem;


        public MyViewHolder(View view) {
            super(view);
            TvSlNo = (TextView) view.findViewById(R.id.tv_item_num);
            TvProductName = (TextView) view.findViewById(R.id.tv_product_name);
            TvQuantity = (EditText) view.findViewById(R.id.tv_quantity);
            TvUnitPrice = (TextView) view.findViewById(R.id.tv_unit_price);
            TvLineTot = (TextView) view.findViewById(R.id.tv_line_total);
            actRemoveItem = (ImageView) view.findViewById(R.id.btn_remove);
            LlItemLayout = (LinearLayout) view.findViewById(R.id.list_head_layout);

        }
    }
}