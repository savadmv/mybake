package com.developers.neoline.mybake.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.adapter.RoutListArrayAdapter;
import com.developers.neoline.mybake.models.Route;
import com.developers.neoline.mybake.util.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class RouteActivity extends AppCompatActivity {
    Button actNext;
    Spinner SpnSelectRout;
    RoutListArrayAdapter adapter;
    ArrayList<Route> route = new ArrayList<>();
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)           // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);

        String token = FirebaseInstanceId.getInstance().getToken();
        Utility.setPrefs("fcm_token", token, getApplicationContext());
        Log.i("Route_activity", "fcm_token-----" + token);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        actNext = (Button) findViewById(R.id.btn_next);
        SpnSelectRout = (Spinner) findViewById(R.id.spn_route_name);

        getDropdownVaue("route", SpnSelectRout);

        String route2 = Utility.getPrefs("route_name", RouteActivity.this);
        if (route2 != null) {

            Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
            startActivity(intent);
            finish();
        }

        //////////action for the next button

        actNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //////Move to next activity//////
//                Crashlytics.getInstance().crash(); // Force a crash
                String route1 = SpnSelectRout.getSelectedItem().toString();
//
                if (route1.contains("Select the Routes")) {

                    setSpinnerError(SpnSelectRout, "Required");
                } else {

                    Route val = route.get(SpnSelectRout.getSelectedItemPosition());
                    Log.i("routeName", val.getrouteName());
                    Utility.setPrefs("route_key", val.getPostKey(), RouteActivity.this);
                    Utility.setPrefs("route_name", SpnSelectRout.getSelectedItem().toString(), RouteActivity.this);
                    Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
                    startActivity(intent);
                    finish();
                }


            }
        });

    }

    private void setSpinnerError(Spinner spinner, String error) {
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError("error"); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }

//    private void getDropdownVaue(String keyvar, final Spinner spinner) {
//        mDatabase.child("dropdownlist").child(keyvar).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
////                Log.i("Main", dataSnapshot.getValue().toString());
//
//                String[] strings = dataSnapshot.getValue(String.class).split(",");
//                setDropdownVaue(strings, spinner);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//    }

    private void getDropdownVaue(String keyvar, final Spinner spinner) {
        mDatabase = FirebaseDatabase.getInstance().getReference(keyvar);
        String key = FirebaseDatabase.getInstance().getReference(keyvar).getKey();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                ArrayList<String> strings = new ArrayList<>();
                ArrayList<Route> temRoute = new ArrayList<>();


                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Route value = dataSnapshot1.getValue(Route.class);
                    Log.i("key", "----->" + dataSnapshot1.getKey());
                    Log.i("shopname", "------>" + value.getrouteName());

                    strings.add(value.getrouteName());
                    temRoute.add(value);

                }
                ArrayList<String> shopNames = new ArrayList<>();
                for (int i = 0; i < strings.size(); i++) {
                    route.add(i, temRoute.get(i));
                    shopNames.add(i, temRoute.get(i).getrouteName());

                }
                setDropdownVaue(shopNames, spinner);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });


    }


    private void setDropdownVaue(List<String> strings, Spinner spinner) {

        final List<String> plantsList = strings;

//        adapter = new RoutListArrayAdapter(this,
//                R.layout.spinner_item,route);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

}
