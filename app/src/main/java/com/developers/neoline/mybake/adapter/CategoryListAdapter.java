package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.activity.ProductsActivity;
import com.developers.neoline.mybake.models.Category;
import com.developers.neoline.mybake.util.Utility;

import java.util.List;

/**
 * Created by savad on 8/1/18.
 */


public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {

    private List<Category> singItemList;
    private Context mContext;
    //    private Spinner SpnShopname;
    private AutoCompleteTextView actvShopeName;


    public CategoryListAdapter(Context context, List<Category> singItemList, AutoCompleteTextView actvShopeName) {
        this.singItemList = singItemList;
        this.mContext = context;
        this.actvShopeName = actvShopeName;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list_single_unit, parent, false);
        return new MyViewHolder(itemView);


    }


    private void setSpinnerError(Spinner spinner, String error) {
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError("error"); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Category singItem = singItemList.get(position);
        holder.TvCategoryName.setText(singItem.getName());

        Log.i("test", "name----" + singItem.getName());


        //item Click listner/////

        holder.CvCategoryItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                String shop = SpnShopname.getSelectedItem().toString();
                String shop = actvShopeName.getText().toString();
//                if (shop.contains("Select the shop name")) {
//
//                    setSpinnerError(SpnShopname, "Required");
//                }


                if (shop.isEmpty()) {

                   actvShopeName.setError("Requiered");
                }
                else {
                    Intent intent = new Intent(mContext, ProductsActivity.class);
                    intent.putExtra("postKey", singItem.getKey());
                    Log.i("productioadpter", "------->" + position);
                    intent.putExtra("posVal", position);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);

                    Utility.setPrefs("shop_name", actvShopeName.getText().toString(), mContext);
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return singItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvCategoryName;
        public CardView CvCategoryItemLayout;


        public MyViewHolder(View view) {
            super(view);
            TvCategoryName = (TextView) view.findViewById(R.id.tv_category_name);
            CvCategoryItemLayout = (CardView) view.findViewById(R.id.cv_category_item_layout);

        }
    }
}