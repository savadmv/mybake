package com.developers.neoline.mybake.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class Shop {

    private String Address;
    private String City;
    private String ContactPerson;
    private String GPSLatitude;
    private String GPSLongitude;
    private String OwnerName;
    private String Phone1;
    private String Phone2;
    private String ShopCode;
    private String ShopDiscount;
    private String ShopName;
    private String State;


    public Shop() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Shop(String Address, String City, String ContactPerson, String GPSLatitude,String GPSLongitude, String OwnerName, String Phone1, String Phone2,String ShopCode, String ShopDiscount, String ShopName, String State) {
        this.Address = Address;
        this.City = City;
        this.ContactPerson = ContactPerson;
        this.GPSLatitude = GPSLatitude;
        this.GPSLongitude = GPSLongitude;
        this.OwnerName = OwnerName;
        this.Phone1 = Phone1;
        this.Phone2 = Phone2;
        this.ShopCode = ShopCode;
        this.ShopDiscount = ShopDiscount;
        this.ShopName = ShopName;
        this.State = State;
    }

    public String getshopName() {
        return ShopName;
    }

    public String getshopCode() {
        return ShopCode;
    }

    public String getownerName() {
        return OwnerName;
    }

    public void setshopName(String ShopName) {
        ShopName = ShopName;
    }
}
// [END post_class]
