package com.developers.neoline.mybake.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.adapter.FinalListAdapter;
import com.developers.neoline.mybake.models.CartList;
import com.developers.neoline.mybake.models.CartThumb;
import com.developers.neoline.mybake.models.Carts;
import com.developers.neoline.mybake.models.Product;
import com.developers.neoline.mybake.util.DBHandler;
import com.developers.neoline.mybake.util.SharedPreference;
import com.developers.neoline.mybake.util.UnicodeFormatter;
import com.developers.neoline.mybake.util.Utility;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class FinalListActivity extends AppCompatActivity implements Runnable {
    private RecyclerView RvFinalList, RvGrFinalList;
    private RecyclerView.Adapter mFinalListAdapter, nFinalListAdapter;
    private List<Carts> selected_normal_product_detail = new ArrayList<>(), selected_gr_product_detail = new ArrayList<>(), selected_product_detail = new ArrayList<>();
    private List<Product> test;
    private SharedPreference sharedPreference;
    private TextView TvSubTotal, TvRouteName, TvShopName, TvNormalTotal, TvGrTotal, TvVatAmount;
    private Button actPrint, actSave;
    private String routeName, shopName, resrouteName, resshopName, curDate, curTime;
    private DatabaseReference mDatabase;
    private DBHandler db;
    private BluetoothSocket mBluetoothSocket;
    private ProgressDialog mBluetoothConnectProgressDialog;
    protected static final String TAG = "TAG";
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    BluetoothDevice mBluetoothDevice;
    private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    BluetoothAdapter mBluetoothAdapter;
    public static byte intToByteArray(int value) {
        byte[] b = ByteBuffer.allocate(4).putInt(value).array();

        for (int k = 0; k < b.length; k++) {
            System.out.println("Selva  [" + k + "] = " + "0x"
                    + UnicodeFormatter.byteToHex(b[k]));
        }

        return b[3];
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_list);

        TvSubTotal = (TextView) findViewById(R.id.tv_sub_total);
        TvRouteName = (TextView) findViewById(R.id.tv_route_name);
        TvShopName = (TextView) findViewById(R.id.tv_shop_name);
        TvNormalTotal = (TextView) findViewById(R.id.tv_normal_total);
        TvGrTotal = (TextView) findViewById(R.id.tv_gr_total);
        TvVatAmount = (TextView) findViewById(R.id.tv_vat_amount);
        actPrint = (Button) findViewById(R.id.btn_print);
        actSave = (Button) findViewById(R.id.btn_save);
        actSave.setEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        routeName = Utility.getPrefs("route_name", FinalListActivity.this);
        shopName = Utility.getPrefs("shop_name", FinalListActivity.this);

        resrouteName = getResources().getString(R.string.route_name);
        resshopName = getResources().getString(R.string.shope_name);

        TvRouteName.setText(resrouteName + " : " + routeName);
        TvShopName.setText(resshopName + " : " + shopName);

        db = new DBHandler(this);

//        Intent intent = getIntent();
//        Bundle args = intent.getBundleExtra("bundle");
//        selected_product_detail = (ArrayList<Carts>) getIntent().getSerializableExtra("cartList");
        sharedPreference = new SharedPreference();

        selected_normal_product_detail = db.getAllNormalCarts();
        selected_gr_product_detail = db.getAllGrCarts();
        selected_product_detail = db.getAllCarts();

        for (Carts carts : selected_normal_product_detail) {

            Log.i("test", "Testing...Normal..." + carts.getLineTotal());
        }

        for (Carts carts : selected_gr_product_detail) {

            Log.i("test", "Testing...Grc..." + carts.getLineTotal());
        }

        for (Carts carts : selected_product_detail) {

            Log.i("test", "Testing....All.." + carts.getLineTotal());
        }

//        selected_product_detail = sharedPreference.getFavorites(this);

//        for (int i = 0; i < db.getShopsCount(); i++) {
//
//            Log.i("DBHandler", "Final activity---" + String.valueOf(selected_product_detail.get(i).getProId()) + "--" + String.valueOf(selected_product_detail.get(i).getProduct_name()) + "--" + String.valueOf(selected_product_detail.get(i).getQTy()) + "--" + String.valueOf(selected_product_detail.get(i).getPrice()) + "--" + String.valueOf(selected_product_detail.get(i).getLineTotal()));
//        }

//        Log.i("test123", "cartlist____________________>" + String.valueOf(selected_product_detail.get(0).getQTy()));
//        Log.i("test123", "cartlist____________________>" + String.valueOf(selected_product_detail.get(1).getQTy()));
//        Log.i("test123", "cartlist____________________>" + String.valueOf(selected_product_detail.get(2).getQTy()));
//        Log.i("test123", "cartlist____________________>" + String.valueOf(selected_product_detail.get(3).getQTy()));
//        Log.i("test123", "cartlist____________________>" + String.valueOf(selected_product_detail.get(4).getQTy()));
////        Log.i("test", "cartlist____________________>" + String.valueOf(selected_product_detail.get(5)));
//        Log.i("test","final----"+selected_product_detail.get(1).getProduct_name());
//        Log.i("test","final----"+selected_product_detail.get(2).getProduct_name());
        ////Definig  the views///////

//        Carts carts = selected_product_detail.get(0);
//        Log.i("FainalList","cartitem1    ----->"+carts);

        RvFinalList = (RecyclerView) findViewById(R.id.rv_final_invoice_list);
        RvGrFinalList = (RecyclerView) findViewById(R.id.rv_final_invoice_list2);


        ////Intiating Adapter/////

        mFinalListAdapter = new FinalListAdapter(FinalListActivity.this, selected_normal_product_detail);
        nFinalListAdapter = new FinalListAdapter(FinalListActivity.this, selected_gr_product_detail);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager nLayoutManager = new LinearLayoutManager(this);
        RvFinalList.setLayoutManager(mLayoutManager);
        RvFinalList.setItemAnimator(new DefaultItemAnimator());
        RvFinalList.setAdapter(mFinalListAdapter);


        RvGrFinalList.setLayoutManager(nLayoutManager);
        RvGrFinalList.setItemAnimator(new DefaultItemAnimator());
        RvGrFinalList.setAdapter(nFinalListAdapter);

//        prepareFinalListData();
        double Normalsum = 0;
        for (int i = 0; i < mFinalListAdapter.getItemCount(); i++) {
            Normalsum = Normalsum + selected_normal_product_detail.get(i).getLineTotal();

        }
        double Grsum = 0;
        for (int i = 0; i < nFinalListAdapter.getItemCount(); i++) {
            Grsum = Grsum + selected_gr_product_detail.get(i).getLineTotal();

        }
        TvNormalTotal.setText(String.valueOf(Normalsum));
        TvGrTotal.setText(String.valueOf(Grsum));
        double NetTot = Normalsum - Grsum;
        double Vat = ((double) NetTot / 100.0f) * 5;
        double amountToPay = (double) NetTot + Vat;
        String formattedValue = String.format("%.2f", amountToPay);
        String formattedVat = String.format("%.2f", Vat);
        TvVatAmount.setText(String.valueOf(formattedVat));
        TvSubTotal.setText(String.valueOf(formattedValue));

        actPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(FinalListActivity.this, "Testing to be done", Toast.LENGTH_LONG).show();
                getDateAndTime();
//                DBHandler db = new DBHandler(FinalListActivity.this);
//                db.deleteAll();
//                Utility.ClearPrefs(getApplicationContext());
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_HOME);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//                finish();

                String key = mDatabase.child("list_content").push().getKey();
                String tempkey = mDatabase.child("list_thumb").child(routeName).push().getKey();

                for (int i = 0; i < db.getShopsCount(); i++) {
                    String subkey = mDatabase.child("list_content").child(key).push().getKey();
//                    Log.i("DBHandler", "Final activity---" + String.valueOf(selected_product_detail.get(i).getProId()) + "--" + String.valueOf(selected_product_detail.get(i).getProduct_name()) + "--" + String.valueOf(selected_product_detail.get(i).getQTy()) + "--" + String.valueOf(selected_product_detail.get(i).getPrice()) + "--" + String.valueOf(selected_product_detail.get(i).getLineTotal()));
                    writeNewPost(key, subkey, selected_product_detail.get(i).getProduct_name(), selected_product_detail.get(i).getQTy(), selected_product_detail.get(i).getPrice(), selected_product_detail.get(i).getLineTotal(), selected_product_detail.get(i).getIsNormal());

                }

                CartThumb thumb = new CartThumb(key, shopName, routeName, curDate, curTime);
                Map<String, Object> postValues = thumb.toMap();

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/list_thumb/" + "/" + routeName + "/" + tempkey, postValues);
                mDatabase.updateChildren(childUpdates);
                Toast.makeText(getApplicationContext(), "Saved Succesfully", Toast.LENGTH_LONG).show();
                actSave.setEnabled(false);

                printFile();

                db.deleteAll();
                Utility.ClearPrefs(getApplicationContext());

            }
        });

        actSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scan();
                String key = mDatabase.child("list_content").push().getKey();
                String tempkey = mDatabase.child("list_thumb").child(routeName).push().getKey();
                getDateAndTime();
                for (int i = 0; i < db.getShopsCount(); i++) {
                    String subkey = mDatabase.child("list_content").child(key).push().getKey();
//                    Log.i("DBHandler", "Final activity---" + String.valueOf(selected_product_detail.get(i).getProId()) + "--" + String.valueOf(selected_product_detail.get(i).getProduct_name()) + "--" + String.valueOf(selected_product_detail.get(i).getQTy()) + "--" + String.valueOf(selected_product_detail.get(i).getPrice()) + "--" + String.valueOf(selected_product_detail.get(i).getLineTotal()));
                    writeNewPost(key, subkey, selected_product_detail.get(i).getProduct_name(), selected_product_detail.get(i).getQTy(), selected_product_detail.get(i).getPrice(), selected_product_detail.get(i).getLineTotal(), selected_product_detail.get(i).getIsNormal());

                }

                CartThumb thumb = new CartThumb(key, shopName, routeName, curDate, curTime);
                Map<String, Object> postValues = thumb.toMap();

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/list_thumb/" + "/" + routeName + "/" + tempkey, postValues);
                mDatabase.updateChildren(childUpdates);
                Toast.makeText(getApplicationContext(), "Saved Succesfully", Toast.LENGTH_LONG).show();
                actSave.setEnabled(false);

                db.deleteAll();
                Utility.ClearPrefs(getApplicationContext());


            }
        });

    }

    private void scan() {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(FinalListActivity.this, "Message1", Toast.LENGTH_SHORT).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent,
                        REQUEST_ENABLE_BT);
            } else {
                ListPairedDevices();
                Intent connectIntent = new Intent(FinalListActivity.this,
                        DeviceListActivity.class);
                startActivityForResult(connectIntent,
                        REQUEST_CONNECT_DEVICE);
            }
        }
    }

    private void printFile() {

        Thread t = new Thread() {
            public void run() {
                try {
                    OutputStream os = mBluetoothSocket
                            .getOutputStream();
                    String BILL = "";

                    BILL = "                   Shop Name:" + shopName + "    \n" +
                            "                   Route:" + routeName + "      \n";
                    BILL = BILL
                            + "-----------------------------------------------\n";


                    BILL = BILL + String.format("%1$-10s %2$10s %3$13s %4$10s %5$10s", "SlNo", "Item", "Qty", "Price", "Line Totel");
                    BILL = BILL + "\n";
                    BILL = BILL
                            + "-----------------------------------------------";
                    int i = 0;
                    for (Carts carts : selected_normal_product_detail) {
                        i++;

                        Log.i("test", "Testing...Normal..." + carts.getLineTotal());
                        BILL = BILL + String.format("%1$-10s %2$10s %3$13s %4$10s %5$10s", i, carts.getProduct_name(), carts.getQTy(), carts.getPrice(), carts.getLineTotal());
                    }


                    BILL = BILL
                            + "\n-----------------------------------------------";
                    BILL = BILL + "\n\n ";


                    BILL = BILL + "                   Total:" + "     " + TvNormalTotal.getText().toString() + "\n";
                    BILL = BILL + "                     Goods return              \n";
                    BILL = BILL + "-----------------------------------------------\n";
                    BILL = BILL + "\n ";

                    BILL = BILL + String.format("%1$-10s %2$10s %3$13s %4$10s %5$10s", "SlNo", "Item", "Qty", "Price", "Line Totel");
                    BILL = BILL + "\n";
                    BILL = BILL
                            + "-----------------------------------------------";
                    int j = 0;
                    for (Carts carts : selected_gr_product_detail) {
                        j++;

                        Log.i("test", "Testing...Normal..." + carts.getLineTotal());
                        BILL = BILL + String.format("%1$-10s %2$10s %3$13s %4$10s %5$10s", j, carts.getProduct_name(), carts.getQTy(), carts.getPrice(), carts.getLineTotal());
                    }


                    BILL = BILL
                            + "\n-----------------------------------------------";
                    BILL = BILL + "\n ";


                    BILL = BILL + "                   Total:" + "     " + TvGrTotal.getText().toString() + "\n";
                    BILL = BILL
                            + "\n-----------------------------------------------";

                    BILL = BILL + "\n\n ";

                    BILL = BILL
                            + "\n-----------------------------------------------";

                    BILL = BILL + "                   Vat(5%):" + "     " + TvVatAmount.getText().toString() + "\n";
                    BILL = BILL + "                   Total:" + "     " + TvSubTotal.getText().toString() + "\n";



                    os.write(BILL.getBytes());
                    //This is printer specific code you can comment ==== > Start

                    // Setting height
                    int gs = 29;
                    os.write(intToByteArray(gs));
                    int h = 104;
                    os.write(intToByteArray(h));
                    int n = 162;
                    os.write(intToByteArray(n));

                    // Setting Width
                    int gs_width = 29;
                    os.write(intToByteArray(gs_width));
                    int w = 119;
                    os.write(intToByteArray(w));
                    int n_width = 2;
                    os.write(intToByteArray(n_width));


                } catch (Exception e) {
                    Log.e("MainActivity", "Exe ", e);
                }
            }
        };
        t.start();

    }

    private void getDateAndTime() {
        String DateAndTime = DateFormat.getDateTimeInstance().format(new Date());

        Calendar calander = Calendar.getInstance();
        int cDay = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        int cYear = calander.get(Calendar.YEAR);
        String selectedMonth = "" + cMonth;
        String selectedYear = "" + cYear;
        int cHour = calander.get(Calendar.HOUR_OF_DAY);
        int cMinute = calander.get(Calendar.MINUTE);
        int cSecond = calander.get(Calendar.SECOND);
        curDate = cYear + "-" + cMonth + "-" + cDay;
        curTime = cHour + ":" + cMinute;
        Log.i("date123", "----" + curDate + "----" + curTime);
    }

    private void writeNewPost(String key, String SubKey, String pro_name, int pro_quantity, String pro_price, double pro_line_total, int isnormal) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously

        String routeName = Utility.getPrefs("route_name", FinalListActivity.this);
        String shopName = Utility.getPrefs("shop_name", FinalListActivity.this);


        CartList post = new CartList(routeName, shopName, pro_name, pro_quantity, pro_price, pro_line_total, isnormal);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/list_content/" + "/" + key + "/" + SubKey, postValues);


        mDatabase.updateChildren(childUpdates);
    }


    public void onActivityResult(int mRequestCode, int mResultCode,
                                 Intent mDataIntent) {
        super.onActivityResult(mRequestCode, mResultCode, mDataIntent);

        switch (mRequestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (mResultCode == Activity.RESULT_OK) {
                    Bundle mExtra = mDataIntent.getExtras();
                    String mDeviceAddress = mExtra.getString("DeviceAddress");
                    Log.v(TAG, "Coming incoming address " + mDeviceAddress);
                    mBluetoothDevice = mBluetoothAdapter
                            .getRemoteDevice(mDeviceAddress);
                    mBluetoothConnectProgressDialog = ProgressDialog.show(this,
                            "Connecting...", mBluetoothDevice.getName() + " : "
                                    + mBluetoothDevice.getAddress(), true, false);
                    Thread mBlutoothConnectThread = new Thread(this);
                    mBlutoothConnectThread.start();
                    // pairToDevice(mBluetoothDevice); This method is replaced by
                    // progress dialog with thread
                }
                break;

            case REQUEST_ENABLE_BT:
                if (mResultCode == Activity.RESULT_OK) {
                    ListPairedDevices();
                    Intent connectIntent = new Intent(FinalListActivity.this,
                            DeviceListActivity.class);
                    startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);
                } else {
                    Toast.makeText(FinalListActivity.this, "Message", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void ListPairedDevices() {
        Set<BluetoothDevice> mPairedDevices = mBluetoothAdapter
                .getBondedDevices();
        if (mPairedDevices.size() > 0) {
            for (BluetoothDevice mDevice : mPairedDevices) {
                Log.v(TAG, "PairedDevices: " + mDevice.getName() + "  "
                        + mDevice.getAddress());
            }
        }
    }

    @Override
    public void run() {

        try {
            mBluetoothSocket = mBluetoothDevice
                    .createRfcommSocketToServiceRecord(applicationUUID);
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothSocket.connect();
            mHandler.sendEmptyMessage(0);
        } catch (IOException eConnectException) {
            Log.d("test", "CouldNotConnectToSocket", eConnectException);
            closeSocket(mBluetoothSocket);
            return;
        }
    }


    private void closeSocket(BluetoothSocket nOpenSocket) {
        try {
            nOpenSocket.close();
            Log.d(TAG, "SocketClosed");
        } catch (IOException ex) {
            Log.d(TAG, "CouldNotCloseSocket");
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mBluetoothConnectProgressDialog.dismiss();
            Toast.makeText(FinalListActivity.this, "DeviceConnected", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        try {
            if (mBluetoothSocket != null)
                mBluetoothSocket.close();
        } catch (Exception e) {
            Log.e("Tag", "Exe ", e);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (mBluetoothSocket != null)
                mBluetoothSocket.close();
        } catch (Exception e) {
            Log.e("Tag", "Exe ", e);
        }
        setResult(RESULT_CANCELED);
        finish();
    }

//    private void prepareFinalListData() {
//
//        Carts carts = new Carts(singItem.getCategoryName(), singItem.getPrice(), holder.qty);
//    }
}
