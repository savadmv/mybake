package com.developers.neoline.mybake.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.activity.RouteActivity;
import com.developers.neoline.mybake.adapter.CategoryListAdapter;
import com.developers.neoline.mybake.models.Category;
import com.developers.neoline.mybake.models.RouteShop;
import com.developers.neoline.mybake.models.Shop;
import com.developers.neoline.mybake.util.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by savad on 18/12/17.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {
    ArrayAdapter<String> shopListAdapterShopName, shopListAdapterShopCode, shopListAdapterOwnerName;
    List<String> payeeNames;
    ArrayList<String> shopName;
    ArrayList<String> shopCode;
    ArrayList<String> shopOwnerName;
    private RecyclerView RvCategoryLis;
    private RecyclerView.Adapter mCategoryListAdapter;
    private List<Category> category_name = new ArrayList<>();
    private DatabaseReference mDatabase, myRef;
    private ProgressBar PbLoading;
    private AutoCompleteTextView actShopName;

//    private Spinner SpnShopName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        getActivity().setTitle(getResources().getString(R.string.app_name));

        mDatabase = FirebaseDatabase.getInstance().getReference("category");
        myRef = FirebaseDatabase.getInstance().getReference();
        ////Definig  the views///////

        RvCategoryLis = (RecyclerView) view.findViewById(R.id.rv_category_list);
        actShopName = (AutoCompleteTextView) view.findViewById(R.id.actv_shopname);
//        SpnShopName = (Spinner) view.findViewById(R.id.spn_shop_name);

        ////Intiating Adapter/////
        PbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        PbLoading.setVisibility(View.VISIBLE);
//        getDropdownVaue("shop", SpnShopName);
        String route_key= Utility.getPrefs("route_key", getActivity());
        Log.i("shopname123", "------>" + route_key);
        getDropdownVaue("route_shop",route_key);

        ArrayList<String> strings = new ArrayList<>();


        mCategoryListAdapter = new CategoryListAdapter(getActivity(), category_name, actShopName);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);

        RvCategoryLis.setLayoutManager(mLayoutManager);
        RvCategoryLis.setItemAnimator(new DefaultItemAnimator());
        RvCategoryLis.setAdapter(mCategoryListAdapter);



        actShopName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if (shopListAdapterShopCode.getCount() == 0 && shopListAdapterOwnerName.getCount() == 0) {
//                    shopListAdapterShopName = new ArrayAdapter<String>(getActivity(),
//                            android.R.layout.select_dialog_item, shopName);
//                    actShopName.setThreshold(1);
//                    actShopName.setAdapter(shopListAdapterShopCode);
//                } else if (shopListAdapterShopName.getCount() == 0 && shopListAdapterOwnerName.getCount() == 0) {
//
//                    shopListAdapterShopCode = new ArrayAdapter<String>(getActivity(),
//                            android.R.layout.select_dialog_item, shopCode);
//                    actShopName.setThreshold(1);
//                    actShopName.setAdapter(shopListAdapterShopCode);
//                } else if (shopListAdapterShopName.getCount() == 0 && shopListAdapterShopCode.getCount() == 0) {
//                    shopListAdapterOwnerName = new ArrayAdapter<String>(getActivity(),
//                            android.R.layout.select_dialog_item, shopOwnerName);
//                    actShopName.setThreshold(1);
//                    actShopName.setAdapter(shopListAdapterOwnerName);
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        prepareCategoryData();

        return view;
    }


    //    private void getDropdownVaue(String keyvar, final Spinner spinner) {
//        myRef.child("dropdownlist").child(keyvar).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
////                Log.i("Main", dataSnapshot.getValue().toString());
//
//                String[] strings = dataSnapshot.getValue(String.class).split(",");
//                setDropdownVaue(strings, spinner);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//    }
    private void getDropdownVaue( String keyvar,String route_key) {
        myRef = FirebaseDatabase.getInstance().getReference(keyvar).child(route_key);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                shopName = new ArrayList<>();
                shopCode = new ArrayList<>();
                shopOwnerName = new ArrayList<>();
//                String code=getActivity().getResources().getString(R.string.code);

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    RouteShop value = dataSnapshot1.getValue(RouteShop.class);

                    Log.i("shopname123", "------>" + value.getshopName());

                    shopName.add(value.getshopName()+" "+value.getshopCode()+" "+value.getshopNumber());
                    shopCode.add(value.getshopName());
                    shopOwnerName.add(value.getshopName());

                }

                shopListAdapterShopName = new ArrayAdapter<String>(getActivity(),
                        android.R.layout.select_dialog_item, shopName);
                actShopName.setThreshold(1);
                actShopName.setAdapter(shopListAdapterShopName);


//                setDropdownVaue(strings, spinner);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });


    }

    private void setDropdownVaue(List<String> List, Spinner spinner) {

        final List<String> plantsList = List;

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, plantsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }

    private void prepareCategoryData() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Category value = dataSnapshot1.getValue(Category.class);
                    Log.i("shopname", value.getName());
                    Category category = new Category();
                    String name = value.getName();
                    String key = value.getKey();
//                    Log.i("test", name);
                    category.setName(name);
                    category.setKey(key);
                    category = new Category(key, name);

                    category_name.add(category);
                    PbLoading.setVisibility(View.GONE);
                    mCategoryListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {

    }
}
