package com.developers.neoline.mybake.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.developers.neoline.mybake.models.Carts;
import com.itextpdf.text.Font;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Savad on 15-12-2017.
 */

public class Utility {


    public static final List<Carts> cartsList = new ArrayList<>();

    public static final List<Carts> carList = null;
    public static final long INTERVAL_ONE_MINUTES = 60000L;
    public static final String PATH_PRODUCT_REPORT = "/MYBAKE/REPORT_PRODUCT/";
    public static Font FONT_BODY = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
    public static Font FONT_HEADER_FOOTER = new Font(Font.FontFamily.UNDEFINED, 7, Font.ITALIC);

    /***** To Generate 6 digit Random Number OTP ****/

    public static void setPrefs(String key, String value, Context context) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPrefs(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static void ClearPrefs(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
    }


}

