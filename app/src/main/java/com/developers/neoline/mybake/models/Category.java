package com.developers.neoline.mybake.models;

import android.support.annotation.Keep;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by savad on 21/12/17.
 */
@Keep
@IgnoreExtraProperties
public class Category {

    public String postKey;
    public String categoryName;


    public Category() {
    }

    public Category(String key, String name) {
        this.categoryName = name;
        this.postKey = key;

    }

    public String getKey() {

        return postKey;
    }

    public void setKey(String postKey) {
        this.postKey = postKey;
    }


    public String getName() {

        return categoryName;
    }

    public void setName(String name) {
        this.categoryName = categoryName;
    }


}
