package com.developers.neoline.mybake.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START post_class]
@IgnoreExtraProperties
public class RouteShop {

    private String DateOfChange;
    private String RouteCode;
    private String ShopCode;
    private String ShopName;
    private String ShopNumber;
    private String Status;


    public RouteShop() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public RouteShop(String DateOfChange, String RouteCode, String ShopCode, String ShopName, String ShopNumber, String Status) {
        this.DateOfChange = DateOfChange;
        this.RouteCode = RouteCode;
        this.ShopCode = ShopCode;
        this.ShopName = ShopName;
        this.ShopNumber = ShopNumber;
        this.Status = Status;
    }

    public String getdateOfChange() {
        return DateOfChange;
    }

    public String getrouteCode() {
        return RouteCode;
    }

    public String getshopCode() {
        return ShopCode;
    }

    public String getshopName() {
        return ShopName;
    }

    public String getshopNumber() {
        return ShopNumber;
    }

    public String getstatus() {
        return Status;
    }
}
// [END post_class]
