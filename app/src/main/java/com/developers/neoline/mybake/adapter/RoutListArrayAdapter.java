package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.developers.neoline.mybake.models.Route;

import java.util.List;

/**
 * Created by savad on 15/3/18.
 */


public class RoutListArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Route> items;
    private final int mResource;

    public RoutListArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                                @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        Log.i("ArrayAdapter","----"+position);
        return createItemView(position, convertView, parent);

    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Log.i("ArrayAdapter","----"+position);
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

//        TextView offTypeTv = (TextView) view.findViewById(R.id.offer_type_txt);
//        TextView numOffersTv = (TextView) view.findViewById(R.id.num_offers_txt);
//        TextView maxDiscTV = (TextView) view.findViewById(R.id.max_discount_txt);
//
//        Offer offerData = items.get(position);
//
//        offTypeTv.setText(offerData.getOfferType());
//        numOffersTv.setText(offerData.getNumberOfCoupons());
//        maxDiscTV.setText(offerData.getMaxDicount());

        return view;
    }
}