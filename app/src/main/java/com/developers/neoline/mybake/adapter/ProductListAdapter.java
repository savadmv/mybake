package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.activity.FinalListActivity;
import com.developers.neoline.mybake.models.Carts;
import com.developers.neoline.mybake.models.Product;
import com.developers.neoline.mybake.util.DBHandler;
import com.developers.neoline.mybake.util.SharedPreference;

import java.util.ArrayList;
import java.util.List;

import static com.developers.neoline.mybake.util.Utility.cartsList;

/**
 * Created by savad on 8/1/18.
 */


public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {


    public DBHandler db;
    SharedPreference sharedPreference;
    private List<Product> singItemList;
    private Context mContext;
    private ImageView finalList;
    private int posVal;
    private TextView TvTotalPrice;
    private List<Carts> selected_normal_product_detail = new ArrayList<>(), selected_gr_product_detail = new ArrayList<>(), selected_product_detail = new ArrayList<>();
    private double normalSum = 0, grcSum = 0;
    private boolean isFoc;


    public ProductListAdapter(final Context context, List<Product> singItemList, int posVal, TextView tvTotalPrice, SwitchCompat focSwitch) {
        this.singItemList = singItemList;
        this.mContext = context;
        this.finalList = finalList;
        this.posVal = posVal;
        this.TvTotalPrice = tvTotalPrice;
//        this.isFoc = isFoc;

        Log.i("log","adapter--"+ String.valueOf(isFoc));
        focSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isFoc=b;
                notifyDataSetChanged();
            }
        });



    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_single_unit, parent, false);
        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Product singItem = singItemList.get(position);
        db = new DBHandler(mContext);
        holder.TvProductName.setText(singItem.getName());
//        Log.i("log","adapter--"+ String.valueOf(isFoc));
//        Log.i("log", singItem.getName());
        holder.SpnItemCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

//                Toast.makeText(mContext,String.valueOf("l-----"+l),Toast.LENGTH_SHORT).show();
                holder.count = i;
                holder.final_count = i;

                if (isFoc){
Log.i("test","---true");
                    if (i == 0) {


                        if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)), 2)) {
                            db.dropCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
                        }
                    } else {

                        if (db.getShopsCount() == 0) {
                            db.addNormalCat(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price), 2);
                        } else {

                            if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)), 2)) {
                                db.updateNormalCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price), 2);
                            } else {
                                db.addNormalCat(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price), 2);
                            }
                        }

                    }
                    refreshTotal();
                }

                else {
                    Log.i("test","---false");
                    if (i == 0) {


                        if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)), 1)) {
                            db.dropCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
                        }
                    } else {

                        if (db.getShopsCount() == 0) {
                            db.addNormalCat(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price), 1);
                        } else {

                            if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)), 1)) {
                                db.updateNormalCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price), 1);
                            } else {
                                db.addNormalCat(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price), 1);
                            }
                        }

                    }
                    refreshTotal();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        holder.SpnGrCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                holder.gr_count = i;
                holder.final_count = i;

                if (i == 0) {


                    if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)), 0)) {
                        db.dropCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
                    }
                } else {

                    if (db.getShopsCount() == 0) {
                        db.addGrCat(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
                    } else {

                        if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)), 0)) {
                            db.updateGrCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
                        } else {
                            db.addGrCat(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
                        }
                    }

                }

////                if (holder.final_count <= 0) {
////                    Toast.makeText(mContext, "Logic Error", Toast.LENGTH_SHORT).show();
////
////                } else {
//
//
//                    db.updateCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.final_count, singItem.price));
//
////                }
                refreshTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

      /*  holder.IvRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.qty == 0) {
                    Toast.makeText(mContext, "Logical Erro", Toast.LENGTH_LONG).show();
                } else {
                    holder.qty = holder.qty - 1;
                    holder.TvItemCount.setText(String.valueOf(holder.qty));

                    if (holder.qty != 0) {
                        singItem.setIsSelected(true);
                    }
                    if (holder.qty == 0) {
                        db.dropCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.qty, singItem.price));


                    }

                    else if (singItem.IsSelected()) {

                        db.updateCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.qty, singItem.price));


                    }

                }
            }
        });
        holder.IvAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.qty == 20) {
                    Toast.makeText(mContext, "Maximum quantity is four", Toast.LENGTH_LONG).show();
                } else {
                    holder.qty = holder.qty + 1;
                    holder.TvItemCount.setText(String.valueOf(holder.qty));

                    if (holder.qty != 0) {
                        singItem.setIsSelected(true);

                    }

                    if (singItem.IsSelected()) {





                        db = new DBHandler(mContext);
                        if (db.getShopsCount() == 0) {
                            db.addShop(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.qty, singItem.price));
                        } else {

                            if (db.Exists(Integer.parseInt(String.valueOf(posVal) + String.valueOf(position)))) {
                                db.updateCart(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.qty, singItem.price));
                            } else {
                                db.addShop(new Carts(String.valueOf(posVal) + String.valueOf(position), singItem.getName(), holder.qty, singItem.price));
                            }
                        }

 private void refreshTotal() {

        DBHandler db = new DBHandler(this);

        selected_normal_product_detail = db.getAllNormalCarts();
        selected_gr_product_detail = db.getAllGrCarts();
        selected_product_detail = db.getAllCarts();

        for (Carts carts : selected_normal_product_detail) {

            normalSum = normalSum + carts.getLineTotal();
        }
        for (Carts carts : selected_gr_product_detail) {

            grcSum = grcSum + carts.getLineTotal();
        }
        double total=normalSum-grcSum;
        String formattedValue = String.format("%.2f", total);
        TvTotalPrice.setText(formattedValue);
    }

                    }

                }
            }
        });
*/

    }

    private void refreshTotal() {

        DBHandler db = new DBHandler(mContext);

        selected_normal_product_detail = db.getAllNormalCarts();
        selected_gr_product_detail = db.getAllGrCarts();
        selected_product_detail = db.getAllCarts();
        normalSum = 0;
        grcSum = 0;

        for (Carts carts : selected_normal_product_detail) {

            normalSum = normalSum + carts.getLineTotal();
        }
        for (Carts carts : selected_gr_product_detail) {

            grcSum = grcSum + carts.getLineTotal();
        }
        double total = normalSum - grcSum;
        String formattedValue = String.format("%.2f", total);
        TvTotalPrice.setText(formattedValue);
    }

    private boolean isTheSame(List<Carts> someList, String proName, String proPrice, int qty, int position) {
        boolean isThesame = true;
        Log.i("productioadpter", "---------somList-------size   " + String.valueOf(someList.size()));
        for (int i = 0; i < someList.size(); i++) {


            Log.i("productioadpter", "---------someList-----getPosVal  " + String.valueOf(someList.get(i).getPosVal()));
            Log.i("productioadpter", "---------posVal-------posVal     " + String.valueOf(posVal));
            Log.i("productioadpter", "---------someList-----getId      " + String.valueOf(someList.get(i).getId()));
            Log.i("productioadpter", "---------posVal-------position   " + String.valueOf(position));
            List<Carts> testList = new ArrayList<>();
            testList = someList;

            if (someList.get(i).getPosVal() == posVal) {
                if (testList.get(i).getId() == position)

                {
                    Carts carts2 = new Carts(proName, proPrice, qty, position, posVal);

                    isThesame = true;
                    cartsList.set(i, carts2);

                    return isThesame;

                    //someList.set(i, someList.get(i).replace(someString, otherString));
                } else {

                    // If it not contains `someString`, add it as it is to newList
                    isThesame = false;
                    return isThesame;
                }

            } else {
                isThesame = false;
                return isThesame;
            }
        }
        return isThesame;
    }

    /*Checks whether a particular product exists in SharedPreferences*/
    public boolean checkFavoriteItem(Carts checkProduct) {
        boolean check = false;
        List<Carts> favorites = sharedPreference.getFavorites(mContext);
        if (favorites != null) {
            for (Carts product : favorites) {
                if (product.equals(checkProduct)) {
                    check = true;
                    break;
                }
            }
        }
        return check;
    }


    @Override
    public int getItemCount() {
        return singItemList.size();
    }

    public List<Carts> getCartsList() {

        return cartsList;
    }

    public interface MyCustomObjectListener {
        // These methods are the different events and
        // need to pass relevant arguments related to the event triggered
//        public void onObjectReady(String title);
        // or when data has been loaded
//        public void onDataLoaded(SomeData data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //        public TextView TvItemCount;
//        public ImageView IvAddItem, IvRemoveItem;
        public int qty = 0;
        public TextView TvProductName;
        public ImageView actFinalList;
        public Spinner SpnItemCount, SpnGrCount;
        public int count = 0, gr_count = 0, final_count = 0;

        public MyViewHolder(View view) {
            super(view);

//            TvItemCount = (TextView) view.findViewById(R.id.tv_count);
            TvProductName = (TextView) view.findViewById(R.id.tv_product_name);

//            IvAddItem = (ImageView) view.findViewById(R.id.iv_plus_count);
//            IvRemoveItem = (ImageView) view.findViewById(R.id.iv_minus_count);
            SpnItemCount = (Spinner) view.findViewById(R.id.spn_count);
            SpnGrCount = (Spinner) view.findViewById(R.id.spn_gr);

            actFinalList = (ImageView) view.findViewById(R.id.btn_final_list);
            sharedPreference = new SharedPreference();

        }


    }
}