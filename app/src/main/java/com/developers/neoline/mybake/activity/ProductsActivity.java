package com.developers.neoline.mybake.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.adapter.ProductListAdapter;
import com.developers.neoline.mybake.models.Carts;
import com.developers.neoline.mybake.models.Product;
import com.developers.neoline.mybake.util.DBHandler;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.developers.neoline.mybake.util.Utility.cartsList;

public class ProductsActivity extends AppCompatActivity {

    public RecyclerView.Adapter mProductListAdapter, adapter;
    private RecyclerView RvProductList;
    private List<Product> product_detail = new ArrayList<>();
    private List<Carts> cartsList = new ArrayList<>();
    private List<Carts> selected_normal_product_detail = new ArrayList<>(), selected_gr_product_detail = new ArrayList<>(), selected_product_detail = new ArrayList<>();
    private ImageView actFinalList;
    private String postKey;
    private int posVal;
    private DatabaseReference mDatabase, myRef;
    private ProgressBar PbLoading;
    private double normalSum = 0, grcSum = 0;
    private TextView TvTotalPrice;
    private SwitchCompat focSwitch;
    private boolean isFoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        postKey = getIntent().getStringExtra("postKey");
        posVal = getIntent().getIntExtra("posVal", 1);
        Log.i("productioadpter", "------->" + posVal);
        mDatabase = FirebaseDatabase.getInstance().getReference("/product/" + postKey);
        TvTotalPrice = findViewById(R.id.tv_total_price);


        DBHandler db = new DBHandler(this);

        selected_normal_product_detail = db.getAllNormalCarts();
        selected_gr_product_detail = db.getAllGrCarts();
        selected_product_detail = db.getAllCarts();


        Log.i("productioadpter", "-count------>" + String.valueOf(db.getShopsCount()));

        ////Definig  the views///////
        PbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        RvProductList = (RecyclerView) findViewById(R.id.rv_product_list);
        actFinalList = (ImageView) findViewById(R.id.btn_final_list);
        focSwitch = (SwitchCompat) findViewById(R.id.sw_foc);
        PbLoading.setVisibility(View.VISIBLE);
        ////Intiating Adapter/////


        mProductListAdapter = new ProductListAdapter(ProductsActivity.this, product_detail, posVal, TvTotalPrice, focSwitch);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 3);

        RvProductList.setLayoutManager(mLayoutManager);
        RvProductList.setItemAnimator(new DefaultItemAnimator());
        RvProductList.setAdapter(mProductListAdapter);

//        focSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                isFoc = b;
//                Log.i("log","activity--"+ String.valueOf(isFoc));
//                mProductListAdapter = new ProductListAdapter(ProductsActivity.this, product_detail, actFinalList, posVal, TvTotalPrice, isFoc);
////                mProductListAdapter.notifyDataSetChanged();
//            }
//        });

//        cartsList = adapter.getCartsList();
        actFinalList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FinalListActivity.class);
                startActivity(intent);
                finish();
            }
        });


        prepareProductData();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void prepareProductData() {


        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Product value = dataSnapshot1.getValue(Product.class);
                    Product product = new Product();
                    String name = value.getName();
                    String price = value.getPrice();
//                    Log.i("test", name);
                    product.setName(name);
                    product.setPrice(price);
                    product = new Product(name, price);

                    product_detail.add(product);
                    PbLoading.setVisibility(View.GONE);
                    mProductListAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });
//        Product product = new Product("Product 1", "2332");
//        product_detail.add(product);
//
//        product = new Product("Product 2", "344");
//        product_detail.add(product);
//
//        product = new Product("Product 3", "404");
//        product_detail.add(product);
//
//        product = new Product("Product 4", "876");
//        product_detail.add(product);
//
//        product = new Product("Product 5", "334");
//        product_detail.add(product);
//
//        product = new Product("Product 6", "1244");
//        product_detail.add(product);


    }


}
