package com.developers.neoline.mybake.models;

import com.google.firebase.database.IgnoreExtraProperties;

// [START post_class]
@IgnoreExtraProperties
public class Route {

    private String CurrentSalesman;
    private String RouteCode;
    private String RouteDescription;
    private String RouteName;
    private String postKey;




    public Route() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Route(String CurrentSalesman, String RouteCode, String RouteDescription, String RouteName,String postKey) {
        this.CurrentSalesman = CurrentSalesman;
        this.RouteCode = RouteCode;
        this.RouteDescription = RouteDescription;
        this.RouteName = RouteName;
        this.postKey=postKey;

    }

    public String getrouteName() {
        return RouteName;
    }

    public void setrouteName(String routeName) {
        RouteName = routeName;
    }

    public String getPostKey() {
        return postKey;
    }
}
// [END post_class]
