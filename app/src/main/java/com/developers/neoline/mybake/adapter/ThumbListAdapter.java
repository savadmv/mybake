package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.activity.ListDetailsActivity;
import com.developers.neoline.mybake.models.CartThumb;
import com.developers.neoline.mybake.util.DBHandler;

import java.util.List;

/**
 * Created by savad on 8/1/18.
 */


public class ThumbListAdapter extends RecyclerView.Adapter<ThumbListAdapter.MyViewHolder> {

    private List<CartThumb> cartItemList;
    private Context mContext;


    public ThumbListAdapter(Context context, List<CartThumb> singItemList) {
        this.cartItemList = singItemList;
        this.mContext = context;
        DBHandler db = new DBHandler(mContext);
//        for (int i = 0; i < db.getShopsCount(); i++) {
//
//            Log.i("DBHandler", "Adapter---" + String.valueOf(singItemList.get(i).getProId()) + "--" + String.valueOf(singItemList.get(i).getProduct_name()) + "--" + String.valueOf(singItemList.get(i).getQTy()) + "--" + String.valueOf(singItemList.get(i).getPrice()) + "--" + String.valueOf(singItemList.get(i).getLineTotal()));
//        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.thumb_single_unit, parent, false);
        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CartThumb thumb = cartItemList.get(position);
        Log.i("ThirdFragment", thumb.getRoute_name() + "----" + thumb.getShop_name());
        holder.TvDate.setText(thumb.getDate());
        holder.TvSlNo.setText(String.valueOf(position + 1));
        holder.Tvshopname.setText(thumb.getShop_name());
        holder.TvTime.setText(thumb.getTime());

        holder.RlThumbLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ListDetailsActivity.class);
                intent.putExtra("key", thumb.getKey());
                mContext.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvSlNo, Tvshopname, TvDate, TvTime;
        public RelativeLayout RlThumbLayout;


        public MyViewHolder(View view) {
            super(view);
            RlThumbLayout = (RelativeLayout) view.findViewById(R.id.rl_thumb_single_unit);
            TvSlNo = (TextView) view.findViewById(R.id.tv_sl_num);
            Tvshopname = (TextView) view.findViewById(R.id.tv_shop_name);
            TvDate = (TextView) view.findViewById(R.id.tv_date);
            TvTime = (TextView) view.findViewById(R.id.tv_time);


        }
    }
}