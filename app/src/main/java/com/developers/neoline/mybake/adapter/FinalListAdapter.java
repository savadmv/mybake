package com.developers.neoline.mybake.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.models.Carts;
import com.developers.neoline.mybake.util.DBHandler;

import java.util.List;

/**
 * Created by savad on 8/1/18.
 */


public class FinalListAdapter extends RecyclerView.Adapter<FinalListAdapter.MyViewHolder> {

    private List<Carts> cartItemList;
    private Context mContext;


    public FinalListAdapter(Context context, List<Carts> singItemList) {
        this.cartItemList = singItemList;
        this.mContext = context;
        DBHandler db = new DBHandler(mContext);
        for (int i = 0; i < db.getShopsCount(); i++) {

//            Log.i("DBHandler", "Adapter---" + String.valueOf(singItemList.get(i).getProId()) + "--" + String.valueOf(singItemList.get(i).getProduct_name()) + "--" + String.valueOf(singItemList.get(i).getQTy()) + "--" + String.valueOf(singItemList.get(i).getPrice()) + "--" + String.valueOf(singItemList.get(i).getLineTotal()));
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.final_list_single_unit, parent, false);
        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Carts carts = cartItemList.get(position);
        holder.TvProductName.setText(carts.getProduct_name());
        holder.TvSlNo.setText(String.valueOf(position + 1));
        holder.TvUnitPrice.setText(carts.getPrice());
        holder.TvQuantity.setText(String.valueOf(carts.getQTy()));
        if (carts.isNormal==2){
            holder.TvLineTot.setText("FOC");

        }
        else {
            holder.TvLineTot.setText(String.valueOf(carts.getLineTotal()));
        }

        Log.i("FianList", "test------>" + carts.getProduct_name());

    }


    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView TvSlNo, TvProductName, TvQuantity, TvUnitPrice, TvLineTot;


        public MyViewHolder(View view) {
            super(view);
            TvSlNo = (TextView) view.findViewById(R.id.tv_item_num);
            TvProductName = (TextView) view.findViewById(R.id.tv_product_name);
            TvQuantity = (TextView) view.findViewById(R.id.tv_quantity);
            TvUnitPrice = (TextView) view.findViewById(R.id.tv_unit_price);
            TvLineTot = (TextView) view.findViewById(R.id.tv_line_total);

        }
    }
}