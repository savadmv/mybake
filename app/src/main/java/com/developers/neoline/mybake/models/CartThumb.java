package com.developers.neoline.mybake.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class CartThumb {

    public String key;
    public String shop_name;
    public String route_name;
    public String date;
    public String time;





    public CartThumb() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public CartThumb(String key, String shop_name, String route_name,String date,String time) {
        this.key = key;
        this.shop_name = shop_name;
        this.route_name = route_name;
        this.date=date;
        this.time=time;

    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("key", key);
        result.put("shop_name", shop_name);
        result.put("route_name", route_name);
        result.put("date",date);
        result.put("time", time);



        return result;
    }
    // [END post_to_map]



    public String getKey() {
        return key;
    }

    public String getShop_name() {
        return shop_name;
    }

    public String getRoute_name() {
        return route_name;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }
}
// [END post_class]
