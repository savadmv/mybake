package com.developers.neoline.mybake.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.developers.neoline.mybake.models.Carts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by savad on 14/1/18.
 */

public class DBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "cartinfo";

    // Contacts table name
    private static final String TABLE_ITEMS = "items";

    // Shops Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PRO_ID = "pro_id";
    private static final String KEY_NAME = "pro_name";
    private static final String KEY_SH_QTY = "qty";
    private static final String KEY_SH_PRICE = "price";
    private static final String KEY_SH_LINE_TOTAL = "line_tot";
    private static final String IS_NORMAL = "is_normal";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_PRO_ID + " INTEGER," + KEY_NAME + " TEXT,"
                + KEY_SH_QTY + " INTEGER," + KEY_SH_PRICE + " TEXT," + KEY_SH_LINE_TOTAL + " INTEGER," + IS_NORMAL + " INTEGER" + ")";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);

    }


    // Adding new shop
    public void addShop(Carts carts) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.i("DBHandler", "add--" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));

        values.put(KEY_PRO_ID, carts.getProId());
        values.put(KEY_NAME, carts.getProduct_name());
        values.put(KEY_SH_QTY, carts.getQTy());
        values.put(KEY_SH_PRICE, carts.getPrice());
        values.put(KEY_SH_LINE_TOTAL, carts.getLineTotal());

        // Inserting Row
        db.insert(TABLE_ITEMS, null, values);
        db.close(); // Closing database connection
    }


    // Adding new shop
    public void addNormalCat(Carts carts,int isNormal) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.i("DBHandler", "add--" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));

        values.put(KEY_PRO_ID, carts.getProId());
        values.put(KEY_NAME, carts.getProduct_name());
        values.put(KEY_SH_QTY, carts.getQTy());
        values.put(KEY_SH_PRICE, carts.getPrice());
        values.put(KEY_SH_LINE_TOTAL, carts.getLineTotal());
        values.put(IS_NORMAL, isNormal);

        // Inserting Row
        db.insert(TABLE_ITEMS, null, values);
        db.close(); // Closing database connection
    }

    public void addGrCat(Carts carts) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.i("DBHandler", "add--" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));

        values.put(KEY_PRO_ID, carts.getProId());
        values.put(KEY_NAME, carts.getProduct_name());
        values.put(KEY_SH_QTY, carts.getQTy());
        values.put(KEY_SH_PRICE, carts.getPrice());
        values.put(KEY_SH_LINE_TOTAL, carts.getLineTotal());
        values.put(IS_NORMAL, 0);

        // Inserting Row
        db.insert(TABLE_ITEMS, null, values);
        db.close(); // Closing database connection
    }


    public int updateNormalCart(Carts carts,int isNormal) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.i("DBHandler", "update---" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));
        ContentValues values = new ContentValues();
        values.put(KEY_PRO_ID, carts.getProId());
        values.put(KEY_NAME, carts.getProduct_name());
        values.put(KEY_SH_QTY, carts.getQTy());
        values.put(KEY_SH_PRICE, carts.getPrice());
        values.put(KEY_SH_LINE_TOTAL, carts.getLineTotal());
        values.put(IS_NORMAL, isNormal);
        Log.i("database", "currentval----->" + String.valueOf(carts.getProId()) + "---" + String.valueOf(carts.getQTy()));

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_PRO_ID + " = ?",
                new String[]{String.valueOf(carts.getProId())});
    }

    public int updateGrCart(Carts carts) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.i("DBHandler", "update---" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));
        ContentValues values = new ContentValues();
        values.put(KEY_PRO_ID, carts.getProId());
        values.put(KEY_NAME, carts.getProduct_name());
        values.put(KEY_SH_QTY, carts.getQTy());
        values.put(KEY_SH_PRICE, carts.getPrice());
        values.put(KEY_SH_LINE_TOTAL, carts.getLineTotal());
        values.put(IS_NORMAL, 0);
        Log.i("database", "currentval----->" + String.valueOf(carts.getProId()) + "---" + String.valueOf(carts.getQTy()));

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_PRO_ID + " = ?",
                new String[]{String.valueOf(carts.getProId())});
    }


//

    // Updating a shop
    public int updateCart(Carts carts) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.i("DBHandler", "update---" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));
        ContentValues values = new ContentValues();
        values.put(KEY_PRO_ID, carts.getProId());
        values.put(KEY_NAME, carts.getProduct_name());
        values.put(KEY_SH_QTY, carts.getQTy());
        values.put(KEY_SH_PRICE, carts.getPrice());
        values.put(KEY_SH_LINE_TOTAL, carts.getLineTotal());
        Log.i("database", "currentval----->" + String.valueOf(carts.getProId()) + "---" + String.valueOf(carts.getQTy()));

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_PRO_ID + " = ?",
                new String[]{String.valueOf(carts.getProId())});
    }


    // Updating a shop
    public int dropCart(Carts carts) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.i("DBHandler", "update---" + String.valueOf(carts.getProId()) + "--" + String.valueOf(carts.getProduct_name()) + "--" + String.valueOf(carts.getQTy()) + "--" + String.valueOf(carts.getPrice()) + "--" + String.valueOf(carts.getLineTotal()));

        Log.i("database", "currentval----->" + String.valueOf(carts.getProId()) + "---" + String.valueOf(carts.getQTy()));

        // updating row
        return db.delete(TABLE_ITEMS, KEY_PRO_ID + " = ?",
                new String[]{String.valueOf(carts.getProId())});
    }

    // Getting shops Count
    public int getShopsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ITEMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        // return count
        return cnt;

    }

    public boolean Exists(int ID, int isnormal) {
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery(selectQuery, null);
        int flag = 0;
        while (res.moveToNext()) {

            int id = res.getInt(1);
            int Normal = res.getInt(6);
            Log.i("database", "currentval----->" + String.valueOf(ID) + "---" + id);
            if (id == ID && Normal == isnormal) {


                flag++;
            }
        }

        if (flag == 0) {
            return false;
        } else {
            return true;
        }
    }


    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();

        //SQLiteDatabase db = this.getWritableDatabase();
        // db.delete(TABLE_NAME,null,null);
        //db.execSQL("delete * from"+ TABLE_NAME);
        db.execSQL("delete from " + TABLE_ITEMS);
        db.close();
    }

    // Getting All Shops
    public List<Carts> getAllCarts() {
        List<Carts> cartList = new ArrayList<Carts>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Carts shop = new Carts();
                Log.i("database", "currentval----->" + String.valueOf(cursor.getInt(3)));
                shop.setId(Integer.parseInt(cursor.getString(0)));
                shop.setProId((cursor.getInt(1)));
                shop.setProduct_name(cursor.getString(2));
                shop.settQuantity(cursor.getInt(3));
                shop.setPrice(cursor.getString(4));
                shop.setLineTotal(cursor.getInt(5));
                shop.setIsNormal(cursor.getInt(6));
                // Adding contact to list
                cartList.add(shop);


            } while (cursor.moveToNext());
        }

        for (int i = 0; i < getShopsCount(); i++) {

            Log.i("DBHandler", "retruve---" + String.valueOf(cartList.get(i).getProId()) + "--" + String.valueOf(cartList.get(i).getProduct_name()) + "--" + String.valueOf(cartList.get(i).getQTy()) + "--" + String.valueOf(cartList.get(i).getPrice()) + "--" + String.valueOf(cartList.get(i).getLineTotal()));
        }
        // return contact list
        return cartList;
    }


    // Getting All Shops
    public List<Carts> getAllNormalCarts() {
        List<Carts> cartList = new ArrayList<Carts>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS + " WHERE " + IS_NORMAL + "=" + 1+" OR "+IS_NORMAL+"="+2;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Carts shop = new Carts();
                Log.i("database", "currentval----->" + String.valueOf(cursor.getInt(3)));
                shop.setId(Integer.parseInt(cursor.getString(0)));
                shop.setProId((cursor.getInt(1)));
                shop.setProduct_name(cursor.getString(2));
                shop.settQuantity(cursor.getInt(3));
                shop.setPrice(cursor.getString(4));
                shop.setLineTotal(cursor.getInt(5));
                shop.setIsNormal(cursor.getInt(6));
                // Adding contact to list
                cartList.add(shop);


            } while (cursor.moveToNext());
        }

        for (int i = 0; i < getShopsCount(); i++) {

//            Log.i("DBHandler", "retruve---" + String.valueOf(cartList.get(i).getProId()) + "--" + String.valueOf(cartList.get(i).getProduct_name()) + "--" + String.valueOf(cartList.get(i).getQTy()) + "--" + String.valueOf(cartList.get(i).getPrice()) + "--" + String.valueOf(cartList.get(i).getLineTotal()));
        }
        // return contact list
        return cartList;
    }


    // Getting All Shops
    public List<Carts> getAllGrCarts() {
        List<Carts> cartList = new ArrayList<Carts>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS + " WHERE " + IS_NORMAL + "=" + 0;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Carts shop = new Carts();
                Log.i("database", "currentval----->" + String.valueOf(cursor.getInt(3)));
                shop.setId(Integer.parseInt(cursor.getString(0)));
                shop.setProId((cursor.getInt(1)));
                shop.setProduct_name(cursor.getString(2));
                shop.settQuantity(cursor.getInt(3));
                shop.setPrice(cursor.getString(4));
                shop.setLineTotal(cursor.getInt(5));
                shop.setIsNormal(cursor.getInt(6));
                // Adding contact to list
                cartList.add(shop);


            } while (cursor.moveToNext());
        }

//        for (int i = 0; i < getShopsCount(); i++) {
//
//            Log.i("DBHandler", "retruve---" + String.valueOf(cartList.get(i).getProId()) + "--" + String.valueOf(cartList.get(i).getProduct_name()) + "--" + String.valueOf(cartList.get(i).getQTy()) + "--" + String.valueOf(cartList.get(i).getPrice()) + "--" + String.valueOf(cartList.get(i).getLineTotal()));
//        }
        // return contact list
        return cartList;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
