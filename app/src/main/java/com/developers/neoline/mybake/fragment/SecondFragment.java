package com.developers.neoline.mybake.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.developers.neoline.mybake.R;
import com.developers.neoline.mybake.activity.RouteActivity;
import com.developers.neoline.mybake.models.Route;
import com.developers.neoline.mybake.util.Utility;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by savad on 18/12/17.
 */

public class SecondFragment extends Fragment implements View.OnClickListener {
    Button actNext;
    Spinner SpnSelectRout;
    private DatabaseReference mDatabase, myRef;
    ArrayList<Route> route = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        getActivity().setTitle("Change The route");


        actNext = (Button) view.findViewById(R.id.btn_next);
        SpnSelectRout = (Spinner) view.findViewById(R.id.spn_route_name);
        myRef = FirebaseDatabase.getInstance().getReference();
        getDropdownVaue("route", SpnSelectRout);

        actNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //////Move to next activity//////
//                Crashlytics.getInstance().crash(); // Force a crash
                String route1 = SpnSelectRout.getSelectedItem().toString();

                if (route1.contains("Select the Routes")) {

                    setSpinnerError(SpnSelectRout, "Required");
                } else {
                    Route val = route.get(SpnSelectRout.getSelectedItemPosition());
                    Log.i("routeName", val.getrouteName());
                    Utility.setPrefs("route_key", val.getPostKey(),getActivity());
                    Utility.setPrefs("route_name", SpnSelectRout.getSelectedItem().toString(), getActivity());
                    Fragment fragment = new HomeFragment();
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.addToBackStack("fragBack");
                    ft.replace(R.id.content_frame, fragment);
                    ft.commit();
                }

            }
        });

        return view;
    }


//        itemListDataAdapter.notifyDataSetChanged();

    private void setSpinnerError(Spinner spinner, String error) {
        View selectedView = spinner.getSelectedView();
        if (selectedView != null && selectedView instanceof TextView) {
            spinner.requestFocus();
            TextView selectedTextView = (TextView) selectedView;
            selectedTextView.setError("error"); // any name of the error will do
            selectedTextView.setTextColor(Color.RED); //text color in which you want your error message to be displayed
            selectedTextView.setText(error); // actual error message
            spinner.performClick(); // to open the spinner list if error is found.

        }
    }
//    private void getDropdownVaue(String keyvar, final Spinner spinner) {
//        myRef.child("dropdownlist").child(keyvar).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
////                Log.i("Main", dataSnapshot.getValue().toString());
//
//                String[] strings = dataSnapshot.getValue(String.class).split(",");
//                setDropdownVaue(strings, spinner);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//    }


    private void getDropdownVaue(String keyvar, final Spinner spinner) {
        mDatabase = FirebaseDatabase.getInstance().getReference(keyvar);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                ArrayList<String> strings = new ArrayList<>();
                ArrayList<Route> temRoute = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    Route value = dataSnapshot1.getValue(Route.class);
                    Log.i("key", "----->" + dataSnapshot1.getKey());
                    Log.i("shopname", "------>" + value.getrouteName());

                    strings.add(value.getrouteName());
                    temRoute.add(value);

                }
                ArrayList<String> shopNames = new ArrayList<>();
                for (int i = 0; i < strings.size(); i++) {
                    route.add(i, temRoute.get(i));
                    shopNames.add(i, temRoute.get(i).getrouteName());

                }
                setDropdownVaue(shopNames, spinner);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Hello", "Failed to read value.", error.toException());
            }
        });


    }

    private void setDropdownVaue(List<String> List, Spinner spinner) {

        final List<String> plantsList =List;

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, plantsList);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(spinnerArrayAdapter);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {

    }
}
